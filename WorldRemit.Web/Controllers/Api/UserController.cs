﻿using System.Web.Http;
using WorldRemit.Services.Interfaces;
using WorldRemit.Services.Models;

namespace WorldRemit.Web.Controllers.Api
{
    [RoutePrefix("api/users")]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Route("all")]
        [HttpPost]
        public IHttpActionResult GetUsers(FilteringOptions model)
        {
            var result = _userService.GetUsers(model);

            if (!result.IsSuccessful)
            {
                return InternalServerError();
            }

            return Ok(result);
        }

        [Route("{userId}/{site}")]
        [HttpGet]
        public IHttpActionResult GetUserDetails(string userId, string site)
        {
            var result = _userService.GetUserDetails(userId, site);

            if (!result.IsSuccessful)
            {
                return InternalServerError();
            }

            return Ok(result);
        }
    }
}