﻿using System.Web.Mvc;

namespace WorldRemit.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}