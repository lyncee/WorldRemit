﻿var $resultLabel, $listingContainer, $dialogContainer;

$(document).ready(function () {

    // Init variables
    $resultLabel = $(document).find('#result');
    $listingContainer = $(document).find('#listing');
    $dialogContainer = $(document).find('#dialog');

    // Load users when the page is loaded
    fetchUsers();
});


// AJAX: Function fetching the users from the API and rendering them using the template.
var fetchUsers = function() {
   
    var data = {
        "PageSize": "20",
        "Sort": "reputation",
        "Order": "desc",
        "Site": "stackoverflow"
    }

    $.ajax({
        url: "/api/users/all",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (result) {
            if (result.Errors.length > 0) {
                showErrorMessage(result.ErrorMessage);
            } else {
                hideMessage();
                displayUsers(result.Data);
                bindEvents();
            }
        },
        error: function (error) {
            showErrorMessage(error);
        }
    });
}

// AJAX: Function fetching a specific user from the API and rendering it using the template.
var fetchUser = function (userId) {

    $.ajax({
        url: "/api/users/" + userId + "/stackoverflow",
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            if (result.Errors.length > 0) {
                showErrorMessage(result.ErrorMessage);
            } else {
                hideMessage();
                displayUserDetails(result.Data);
            }
        },
        error: function (error) {
            showErrorMessage(error);
        }
    });
}

// Function binding the user events 
var bindEvents = function () {

    // Event when we click on one user panel
    $(document).on('click', '.panel.panel-default', function () {
        // We get the ID of this specific user
        var userId = $(this).attr("id");
        // We retrieve its details
        fetchUser(userId);
    });
}


// Function merging the users listing data in the template and filling the container
var displayUsers = function (data) {
    $listingContainer.html(tmpl("tmpl-userlist", data));
}

// Function merging the user details data in the template and filling the containe, also opens the modal at the end
var displayUserDetails = function (data) {
    $dialogContainer.html(tmpl("tmpl-userdetails", data));
    $dialogContainer.dialog({
        modal: true,
        minWidth: 800,
        minHeight: 600,
        resizable: false
    });
}

// Function displaying an error message 
var showErrorMessage = function (message) {
    $resultLabel.show();
    $resultLabel.html(message);
}

// Function hiding the error message
var hideMessage = function () {
    $resultLabel.hide();
    $resultLabel.empty();
}