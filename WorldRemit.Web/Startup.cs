﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using WorldRemit.Services.Implementations;
using WorldRemit.Services.Interfaces;
using WorldRemit.Services.Models.Validators;
using WorldRemit.Shared.Helpers;
using WorldRemit.Shared.Wrappers;

[assembly: OwinStartupAttribute(typeof(WorldRemit.Web.Startup))]
namespace WorldRemit.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var builder = new ContainerBuilder();

            // Register API controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register dependencies
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<ConfigurationHelper>().As<IConfigurationHelper>().InstancePerLifetimeScope();
            builder.RegisterType<HttpClientHelper>().As<IHttpClientHelper>().InstancePerLifetimeScope();
            builder.RegisterType<HttpHelper>().As<IHttpHelper>().InstancePerLifetimeScope();
            builder.RegisterType<LogHelper>().As<ILogHelper>().InstancePerLifetimeScope();
            builder.RegisterType<JsonConvertWrapper>().As<IJsonConvertWrapper>().InstancePerLifetimeScope();
            builder.RegisterType<RestClientWrapper>().As<IRestClientWrapper>().InstancePerLifetimeScope();
            builder.RegisterType<FilteringOptionsValidator>().As<IFilteringOptionsValidator>().InstancePerLifetimeScope();

            var container = builder.Build();

            // HttpConfiguration
            var config = new HttpConfiguration { DependencyResolver = new AutofacWebApiDependencyResolver(container) };
            WebApiConfig.Register(config);

            // Enable CORS
            appBuilder.UseCors(CorsOptions.AllowAll);

            appBuilder.UseAutofacMiddleware(container);
            appBuilder.UseAutofacWebApi(config);
            appBuilder.UseWebApi(config);

        }
    }
}
