﻿using System.Collections.Generic;
using System.Linq;

namespace WorldRemit.Shared.Common
{
    /// <summary>
    /// Object representing the result of an operation
    /// </summary>
    public class Result
    {
        public Result()
        {
            Errors = new List<string>();
        }

        /// <summary>
        /// The text message of this result
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The list of errors
        /// </summary>
        public List<string> Errors { get; set; }

        /// <summary>
        /// The state of this result: true for successful, false for unsuccessful
        /// </summary>
        public bool IsSuccessful => !Errors.Any();

        /// <summary>
        /// A flat error message created from the list of errors
        /// </summary>
        public string Error => string.Join(", ", Errors.ToArray());
    }

    /// <summary>
    /// Object representing the result of an operation and containing custom data
    /// </summary>
    public class Result<T> : Result where T : class
    {
        /// <summary>
        /// Optional data returned 
        /// </summary>
        public T Data { get; set; }
    }

}
