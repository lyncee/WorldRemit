﻿namespace WorldRemit.Shared.Constants
{
    public static class Order
    {
        public static string Asc => "asc";

        public static string Desc => "desc";
    }
}
