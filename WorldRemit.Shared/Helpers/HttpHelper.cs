﻿using System;
using System.Linq;
using System.Text;
using WorldRemit.Shared.Common;

namespace WorldRemit.Shared.Helpers
{
    /// <summary>
    /// Helper providing functionalities related to the Http protocol
    /// </summary>
    public interface IHttpHelper
    {
        /// <summary>
        /// This function will take an object and parse it to a compatible querystring
        /// </summary>
        /// <param name="obj">object to use to append properties and values</param>
        /// <returns>string</returns>
        Result<string> AppendQueryStringParameters(object obj);
    }

    /// <summary>
    /// Concrete implementation of IHttpHelper
    /// </summary>
    public class HttpHelper : IHttpHelper
    {
        private readonly ILogHelper _logHelper;

        public HttpHelper(ILogHelper logHelper)
        {
            _logHelper = logHelper;
        }

        /// <summary>
        /// This function will take an object and parse it to a compatible querystring
        /// </summary>
        /// <param name="obj">object to use to append properties and values</param>
        /// <returns>string</returns>
        public Result<string> AppendQueryStringParameters(object obj)
        {
            var result = new Result<string>();

            try
            {
                if (obj == null)
                {
                    result.Errors.Add("Object is null.");
                    return result;
                }

                var queryString = new StringBuilder();

                var properties = obj.GetType().GetProperties().Where(p => p.GetValue(obj, null) != null).OrderBy(p => p.Name);

                if (properties.Any())
                {
                    queryString.Append("?");
                }

                foreach (var property in properties)
                {
                    var value = property.GetValue(obj, null).ToString();

                    if (!string.IsNullOrEmpty(value))
                    {
                        queryString.Append(property.Name.ToLower() + "=" + value.ToLower() + "&");
                    }
                }

                if (queryString.Length > 1)
                {
                    queryString = queryString.Remove(queryString.Length - 1, 1);
                }

                result.Data = queryString.ToString();
            }
            catch (Exception ex)
            {
                _logHelper.Exception(ex);
                result.Errors.Add(ex.Message);
            }

            return result;
        }
    }
}
