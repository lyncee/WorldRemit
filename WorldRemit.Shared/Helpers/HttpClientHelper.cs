﻿using System;
using WorldRemit.Shared.Common;
using WorldRemit.Shared.Wrappers;

namespace WorldRemit.Shared.Helpers
{
    /// <summary>
    /// Helper wrapping functionalities around the .NET HttpClient
    /// </summary>
    public interface IHttpClientHelper
    {
        /// <summary>
        /// Sends a request to a specific endpoint using the GET method and parse the response to the type T
        /// </summary>
        /// <typeparam name="T">Model used to deserialize the response</typeparam>
        /// <param name="endpointUri">Uri of the external service</param>
        /// <returns>Result object</returns>
        Result<T> Get<T>(string endpointUri) where T : class;

        /// <summary>
        /// Sends a request to a specific endpoint using the POST method and parse the response to the type T
        /// </summary>
        /// <typeparam name="T">Model used to deserialize the response</typeparam>
        /// <param name="endpointUri">Uri of the external service</param>
        /// <param name="data">Payload to send with the request</param>
        /// <returns>Result object</returns>
        Result<T> Post<T>(string endpointUri, object data) where T : class;
    }

    /// <summary>
    /// Concrete implementation of IHttpClientHelper
    /// </summary>
    public class HttpClientHelper : IHttpClientHelper
    {
        private readonly IConfigurationHelper _configurationHelper;
        private readonly ILogHelper _logHelper;
        private readonly IJsonConvertWrapper _jsonConvertWrapper;
        private readonly IRestClientWrapper _restClientWrapper;

        public HttpClientHelper(IConfigurationHelper configurationHelper, ILogHelper logHelper, IJsonConvertWrapper jsonConvertWrapper, IRestClientWrapper restClientWrapper)
        {
            _configurationHelper = configurationHelper;
            _logHelper = logHelper;
            _jsonConvertWrapper = jsonConvertWrapper;
            _restClientWrapper = restClientWrapper;
        }

        /// <summary>
        /// Sends a request to a specific endpoint using the GET method and parse the response to the type T
        /// </summary>
        /// <typeparam name="T">Model used to deserialize the response</typeparam>
        /// <param name="endpointUri">Uri of the external service</param>
        /// <returns>Result object</returns>
        public Result<T> Get<T>(string endpointUri) where T : class
        { 
            var result = new Result<T>();

            try
            {
                var baseUri = _configurationHelper.ApiRootUri;

                var response = _restClientWrapper.Get(baseUri, endpointUri);

                result.Data = _jsonConvertWrapper.Deserialize<T>(response);
            }
            catch (Exception ex)
            {
                _logHelper.Exception(ex);
                result.Errors.Add(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Sends a request to a specific endpoint using the POST method and parse the response to the type T
        /// </summary>
        /// <typeparam name="T">Model used to deserialize the response</typeparam>
        /// <param name="endpointUri">Uri of the external service</param>
        /// <param name="data">Payload to send with the request</param>
        /// <returns>Result object</returns>
        public Result<T> Post<T>(string endpointUri, object data) where T : class
        {
            var result = new Result<T>();

            try
            {
                var baseUri = _configurationHelper.ApiRootUri;

                var response = _restClientWrapper.Post(baseUri, endpointUri, data);

                result.Data = _jsonConvertWrapper.Deserialize<T>(response);
            }
            catch (Exception ex)
            {
                _logHelper.Exception(ex);
                result.Errors.Add(ex.Message);
            }

            return result;
        }
    }
}
