﻿using System;

namespace WorldRemit.Shared.Helpers
{
    /// <summary>
    /// Helper to manage logging
    /// </summary>
    public interface ILogHelper
    {
        void Info(string message);

        void Warning(string message);

        void Error(string message);

        void Exception(Exception ex);
    }

    /// <summary>
    /// Concrete implementation of ILogHelper
    /// </summary>
    public class LogHelper : ILogHelper
    {
        /// <summary>
        /// Write a log as a system information
        /// </summary>
        /// <param name="message"></param>
        public void Info(string message)
        {
            Write($"INFO : {message}");
        }

        /// <summary>
        /// Write a log as a warning
        /// </summary>
        /// <param name="message"></param>
        public void Warning(string message)
        {
            Write($"WARNING : {message}");
        }

        /// <summary>
        /// Write a log as an handled error
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            Write($"ERROR : {message}");
        }

        /// <summary>
        /// Write a log as a thrown exception
        /// </summary>
        /// <param name="ex"></param>
        public void Exception(Exception ex)
        {
            Write($"EXCEPTION : {ex.Message} - {ex.StackTrace}");
        }

        private void Write(string message)
        {
            // Create the message here
            var log = $"{DateTime.Now:dd/MM/yyy hh:mm:ss}: {message}";

            //TODO: Write the log in the DB here

        }
    }
}
