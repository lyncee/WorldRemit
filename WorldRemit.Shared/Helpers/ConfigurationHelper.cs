﻿using System.Configuration;

namespace WorldRemit.Shared.Helpers
{
    public interface IConfigurationHelper
    {
        string ApiRootUri { get; }

        string UsersRelativeUri { get; }
    }

    public class ConfigurationHelper : IConfigurationHelper
    {
        public string ApiRootUri => ConfigurationManager.AppSettings["ApiRootUri"];

        public string UsersRelativeUri =>  ConfigurationManager.AppSettings["UsersRelativeUri"];
    }
}
