﻿using Newtonsoft.Json;
using WorldRemit.Shared.Helpers;

namespace WorldRemit.Shared.Wrappers
{
    /// <summary>
    /// Wrapper around JsonConvert
    /// </summary>
    public interface IJsonConvertWrapper
    {
        T Deserialize<T>(string response);
    }

    /// <summary>
    /// Concrete implementation of IJsonConvertWrapper
    /// </summary>
    public class JsonConvertWrapper : IJsonConvertWrapper
    {
        public T Deserialize<T>(string response)
        {
            return JsonConvert.DeserializeObject<T>(response, new EpochDateConverter());
        }
    }
}
