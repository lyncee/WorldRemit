﻿using RestSharp;

namespace WorldRemit.Shared.Wrappers
{
    public interface IRestClientWrapper
    {
        string Get(string baseUri, string endpointUri);

        string Post(string baseUri, string endPointUri, object data);
    }

    public class RestClientWrapper : IRestClientWrapper
    {
        public string Get(string baseUri, string endpointUri)
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest(endpointUri, Method.GET);
            return client.Execute(request).Content;
        }

        public string Post(string baseUri, string endPointUri, object data)
        {
            var client = new RestClient(baseUri);
            var request = new RestRequest(endPointUri, Method.POST)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddBody(data);
            client.Execute(request);

            return client.Execute(request).Content;
        }
    }
}
