﻿using System;

namespace WorldRemit.Shared.Extensions
{
    public static class DateTimeExtensions
    {
        public static string Clean(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy HH:mm");
        }
    }
}
