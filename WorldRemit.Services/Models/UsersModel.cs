﻿using System.Collections.Generic;

namespace WorldRemit.Services.Models
{
    public class UsersModel
    {
        public List<UserModel> Users { get; set; }
    }
}
