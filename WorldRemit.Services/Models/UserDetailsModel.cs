﻿namespace WorldRemit.Services.Models
{
    public class UserDetailsModel : UserModel
    {
        public string Location { get; set; }

        public string CreationDate { get; set; }
    }
}
