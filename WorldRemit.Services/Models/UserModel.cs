﻿namespace WorldRemit.Services.Models
{
    public class UserModel
    {
        public string UserId { get; set; }

        public string ImageUrl { get; set; }

        public string Name { get; set; }

        public string Reputation { get; set; }
    }
}
