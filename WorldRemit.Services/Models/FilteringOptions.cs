﻿namespace WorldRemit.Services.Models
{
    public class FilteringOptions
    {
        public string PageSize { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }

        public string Site { get; set; }
    }
}
