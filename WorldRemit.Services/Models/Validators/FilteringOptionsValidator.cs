﻿using WorldRemit.Shared.Common;

namespace WorldRemit.Services.Models.Validators
{
    /// <summary>
    /// Concrete implementation of IHttpClientHelper
    /// </summary>
    public interface IFilteringOptionsValidator
    {
        /// <summary>
        /// Validates a FilteringOptions
        /// </summary>
        /// <param name="model">the FilteringOptions to validate</param>
        /// <returns>true if valid, false otherwise</returns>
        Result Validate(FilteringOptions model);
    }

    /// <summary>
    /// Concrete implementation of IHttpClientHelper
    /// </summary>
    public class FilteringOptionsValidator : IFilteringOptionsValidator
    {
        // TODO: Remark => Could have used a library like FluentValidation here to simplify the logic.
        /// <summary>
        /// Validates a FilteringOptions
        /// </summary>
        /// <param name="model">the FilteringOptions to validate</param>
        /// <returns>true if valid, false otherwise</returns>
        public Result Validate(FilteringOptions model)
        {
            var result = new Result();

            if (string.IsNullOrEmpty(model.PageSize))
            {
                result.Errors.Add("Missing pageSize parameter.");
            }

            if (string.IsNullOrEmpty(model.Order))
            {
                result.Errors.Add("Missing order parameter.");
            }

            if (string.IsNullOrEmpty(model.Sort))
            {
                result.Errors.Add("Missing sort parameter.");
            }

            if (string.IsNullOrEmpty(model.Site))
            {
                result.Errors.Add("Missing site parameter.");
            }

            return result;
        }
    }
}
