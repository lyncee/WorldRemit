﻿using WorldRemit.Services.Models;
using WorldRemit.Shared.Common;

namespace WorldRemit.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Retrieves a list of users
        /// </summary>
        /// <param name="options"></param>
        /// <returns>UsersModel object</returns>
        Result<UsersModel> GetUsers(FilteringOptions options);

        /// <summary>
        /// Retrieves a specific user
        /// </summary>
        /// <param name="userId">The user unique identifier</param>
        /// <param name="site">Site</param>
        /// <returns>UserDetailsModel object</returns>
        Result<UserDetailsModel> GetUserDetails(string userId, string site);
    }
}
