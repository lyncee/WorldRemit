﻿using System;
using System.Linq;
using WorldRemit.Services.DTOs.Response;
using WorldRemit.Services.Interfaces;
using WorldRemit.Services.Models;
using WorldRemit.Services.Models.Validators;
using WorldRemit.Shared.Common;
using WorldRemit.Shared.Extensions;
using WorldRemit.Shared.Helpers;

namespace WorldRemit.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IFilteringOptionsValidator _filteringOptionsValidator;
        private readonly IConfigurationHelper _configurationHelper;
        private readonly IHttpHelper _httpHelper;
        private readonly IHttpClientHelper _httpClientHelper;
        private readonly ILogHelper _logHelper;

        public UserService(IFilteringOptionsValidator filteringOptionsValidator,
                           IConfigurationHelper configurationHelper, 
                           IHttpHelper httpHelper, 
                           IHttpClientHelper httpClientHelper, 
                           ILogHelper logHelper)
        {
            _filteringOptionsValidator = filteringOptionsValidator;
            _configurationHelper = configurationHelper;
            _httpHelper = httpHelper;
            _httpClientHelper = httpClientHelper;
            _logHelper = logHelper;
        }

        /// <summary>
        /// Retrieves a list of users
        /// </summary>
        /// <param name="options">Filtering options</param>
        /// <returns>UsersModel object</returns>
        public Result<UsersModel> GetUsers(FilteringOptions options)
        {
            var result = new Result<UsersModel>();

            try
            {
                var validationResult = _filteringOptionsValidator.Validate(options);
                if (!validationResult.IsSuccessful)
                {
                    result.Errors = validationResult.Errors;
                    return result;
                }

                var uri = _configurationHelper.UsersRelativeUri;
                if (string.IsNullOrEmpty(uri))
                {
                    result.Errors.Add("Missing users uri.");
                    return result;
                }

                var queryStringGenerationResult = _httpHelper.AppendQueryStringParameters(options);
                if (!queryStringGenerationResult.IsSuccessful)
                {
                    result.Errors = queryStringGenerationResult.Errors;
                    return result;
                }

                var requestResponse = _httpClientHelper.Get<Users>(uri + queryStringGenerationResult.Data);
                if (!requestResponse.IsSuccessful)
                {
                    result.Errors = requestResponse.Errors;
                    return result;
                }

                result.Data = new UsersModel
                {
                    Users = requestResponse.Data.Items.Select(x => new UserModel
                    {
                        ImageUrl = x.ProfileImage,
                        Name = x.DisplayName,
                        Reputation = x.Reputation.ToString(),
                        UserId = x.UserId.ToString()
                    }).ToList()
                };
            }
            catch (Exception ex)
            {
                _logHelper.Exception(ex);
                result.Errors.Add(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Retrieves a specific user
        /// </summary>
        /// <param name="userId">The user unique identifier</param>
        /// <param name="site">Site</param>
        /// <returns>UserDetailsModel object</returns>
        public Result<UserDetailsModel> GetUserDetails(string userId, string site)
        {
            var result = new Result<UserDetailsModel>();

            try
            {
                if (string.IsNullOrEmpty(userId))
                {
                    result.Errors.Add("Missing userId.");
                    return result;
                }

                if (string.IsNullOrEmpty(site))
                {
                    result.Errors.Add("Missing site.");
                    return result;
                }

                var uri = _configurationHelper.UsersRelativeUri;
                if (string.IsNullOrEmpty(uri))
                {
                    result.Errors.Add("Missing users uri.");
                    return result;
                }

                var requestResponse = _httpClientHelper.Get<Users>(uri + "/" + userId + $"?site={site}");
                if (!requestResponse.IsSuccessful)
                {
                    result.Errors = requestResponse.Errors;
                    return result;
                }

                var user = requestResponse.Data.Items.FirstOrDefault();
                if (user == null)
                {
                    result.Errors.Add($"User {userId} could not be found.");
                    return result;
                }

                result.Data = new UserDetailsModel
                {
                    ImageUrl = user.ProfileImage,
                    Name = user.DisplayName,
                    Reputation = user.Reputation.ToString(),
                    UserId = user.UserId.ToString(),
                    CreationDate = user.CreationDate.Clean(),
                    Location = user.Location
                };
            }
            catch (Exception ex)
            {
                _logHelper.Exception(ex);
                result.Errors.Add(ex.Message);
            }

            return result;
        }
    }
}
