﻿using Newtonsoft.Json;

namespace WorldRemit.Services.DTOs.Response
{
    public class Badge
    {
        [JsonProperty(PropertyName = "bronze")]
        public int Bronze { get; set; }

        [JsonProperty(PropertyName = "gold")]
        public int Gold { get; set; }

        [JsonProperty(PropertyName = "silver")]
        public int Silver { get; set; }
    }
}
