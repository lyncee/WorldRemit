﻿using Newtonsoft.Json;
using System;

namespace WorldRemit.Services.DTOs.Response
{
    public class User
    {
        [JsonProperty(PropertyName = "accept_rate")]
        public int AcceptRate { get; set; }

        [JsonProperty(PropertyName = "account_id")]
        public int AccountId { get; set; }

        [JsonProperty(PropertyName = "age")]
        public int Age { get; set; }

        [JsonProperty(PropertyName = "badge_counts")]
        public Badge BadgeCounts { get; set; }

        [JsonProperty(PropertyName = "creation_date")]
        public DateTime CreationDate { get; set; }

        [JsonProperty(PropertyName = "display_name")]
        public string DisplayName { get; set; }

        [JsonProperty(PropertyName = "is_employee")]
        public bool IsEmployee { get; set; }

        [JsonProperty(PropertyName = "last_access_date")]
        public DateTime LastAccessDate { get; set; }

        [JsonProperty(PropertyName = "last_modified_date")]
        public DateTime LastModifiedDate { get; set; }

        [JsonProperty(PropertyName = "link")]
        public string Link { get; set; }

        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "profile_image")]
        public string ProfileImage { get; set; }

        [JsonProperty(PropertyName = "reputation")]
        public int Reputation { get; set; }

        [JsonProperty(PropertyName = "reputation_change_day")]
        public string ReputationChangeDay { get; set; }

        [JsonProperty(PropertyName = "reputation_change_month")]
        public string ReputationChangeMonth { get; set; }

        [JsonProperty(PropertyName = "reputation_change_quarter")]
        public string ReputationChangeQuarter { get; set; }

        [JsonProperty(PropertyName = "reputation_change_week")]
        public string ReputationChangeWeek { get; set; }

        [JsonProperty(PropertyName = "reputation_change_year")]
        public string ReputationChangeYear { get; set; }

        [JsonProperty(PropertyName = "user_id")]
        public int UserId { get; set; }

        [JsonProperty(PropertyName = "user_type")]
        public string UserType { get; set; }

        [JsonProperty(PropertyName = "website_url")]
        public string WebsiteUrl { get; set; }
    }
}
