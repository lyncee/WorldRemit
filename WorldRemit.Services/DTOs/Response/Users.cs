﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WorldRemit.Services.DTOs.Response
{
    public class Users
    {
        [JsonProperty(PropertyName = "has_more")]
        public bool HasMore { get; set; }

        [JsonProperty(PropertyName = "items")]
        public List<User> Items { get; set; }

        [JsonProperty(PropertyName = "quota_max")]
        public int QuotaMax { get; set; }

        [JsonProperty(PropertyName = "quota_remaining")]
        public int QuotaRemaining { get; set; }
    }
}
