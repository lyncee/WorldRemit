﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WorldRemit.Services.DTOs.Response;
using WorldRemit.Services.Implementations;
using WorldRemit.Services.Models.Validators;
using WorldRemit.Shared.Common;
using WorldRemit.Shared.Helpers;

namespace WorldRemit.Tests.Services.UserServiceTests
{
    [TestFixture]
    public class GetUserDetailsTests
    {
        private readonly string _usersUriTest = @"http://api.stackexchange.com/2.2/users";
        private Mock<IFilteringOptionsValidator> _getUsersModelValidator;
        private Mock<IConfigurationHelper> _configurationHelperMock;
        private Mock<IHttpHelper> _httpHelperMock;
        private Mock<IHttpClientHelper> _httpClientHelperMock;
        private Mock<ILogHelper> _logHelperMock;
        private UserService SUT;

        [SetUp]
        public void Setup()
        {
            _getUsersModelValidator = new Mock<IFilteringOptionsValidator>();
            _configurationHelperMock = new Mock<IConfigurationHelper>();
            _httpHelperMock = new Mock<IHttpHelper>();
            _httpClientHelperMock = new Mock<IHttpClientHelper>();
            _logHelperMock = new Mock<ILogHelper>();
        }

        private void InitServiceUnderTest()
        {
            SUT = new UserService(_getUsersModelValidator.Object,
                                  _configurationHelperMock.Object,
                                  _httpHelperMock.Object,
                                  _httpClientHelperMock.Object,
                                  _logHelperMock.Object);
        }

        [Test]
        public void ensure_returns_true_and_user_when_successful()
        {
            // Arrange
            var userId = "1";

            var site = "stackoverflow";

            var expectedUri = _usersUriTest;

            var expectedFinalUri = expectedUri + "/" + userId + $"?site={site}";

            var response = new Result<Users>
            {
                Data = new Users
                {
                    Items = new List<User> { new User() }
                } 
            };


            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            _httpClientHelperMock.Setup(x => x.Get<Users>(expectedFinalUri)).Returns(response);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUserDetails(userId, site);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessful);

            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<Users>(expectedFinalUri), Times.Once);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_id_missing()
        {
            // Arrange
            var userId = "";

            var site = "stackoverflow";

            var expectedError = "Missing userId.";

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUserDetails(userId, site);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
            Assert.IsNull(result.Data);

            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Never);
            _httpClientHelperMock.Verify(x => x.Get<Users>(It.IsAny<string>()), Times.Never);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_site_missing()
        {
            // Arrange
            var userId = "1";

            var site = "";

            var expectedError = "Missing site.";

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUserDetails(userId, site);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
            Assert.IsNull(result.Data);

            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Never);
            _httpClientHelperMock.Verify(x => x.Get<User>(It.IsAny<string>()), Times.Never);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_uri_not_found()
        {
            // Arrange
            var userId = "1";

            var site = "stackoverflow";

            var expectedUri = "";

            var expectedError = "Missing users uri.";


            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUserDetails(userId, site);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
            Assert.IsNull(result.Data);

            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<User>(It.IsAny<string>()), Times.Never);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_user_not_found()
        {
            // Arrange
            var userId = "1";

            var site = "stackoverflow";

            var expectedUri = _usersUriTest;

            var expectedError = $"User {userId} could not be found.";

            var expectedFinalUri = expectedUri + "/" + userId + $"?site={site}";

            var response = new Result<Users>
            {
                Data = new Users
                {
                    Items = new List<User>()
                }
            };

            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            _httpClientHelperMock.Setup(x => x.Get<Users>(expectedFinalUri)).Returns(response);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUserDetails(userId, site);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
            Assert.IsNull(result.Data);

            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<Users>(It.IsAny<string>()), Times.Once);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_request_returns_error()
        {
            // Arrange
            var userId = "1";

            var site = "stackoverflow";

            var expectedUri = _usersUriTest;

            var expectedFinalUri = expectedUri + "/" + userId + $"?site={site}";

            var expectedError = "An error has occured.";

            var response = new Result<Users>
            {
                Errors = new List<string> { expectedError }
            };


            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            _httpClientHelperMock.Setup(x => x.Get<Users>(expectedFinalUri)).Returns(response);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUserDetails(userId, site);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
            Assert.IsNull(result.Data);

            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<Users>(expectedFinalUri), Times.Once);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_and_user_when_exception_thrown()
        {
            // Arrange
            var userId = "1";

            var site = "stackoverflow";

            var expectedUri = _usersUriTest;

            var expectedFinalUri = expectedUri + "/" + userId + $"?site={site}";

            var expectedErrorMessage = "An exception has occured.";

            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            _httpClientHelperMock.Setup(x => x.Get<Users>(expectedFinalUri)).Throws(new Exception(expectedErrorMessage));

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUserDetails(userId, site);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedErrorMessage, result.Errors);
            Assert.IsNull(result.Data);

            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<Users>(expectedFinalUri), Times.Once);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Once);
        }
    }
}
