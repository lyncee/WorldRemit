﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WorldRemit.Services.DTOs.Response;
using WorldRemit.Services.Implementations;
using WorldRemit.Services.Models;
using WorldRemit.Services.Models.Validators;
using WorldRemit.Shared.Common;
using WorldRemit.Shared.Constants;
using WorldRemit.Shared.Helpers;

namespace WorldRemit.Tests.Services.UserServiceTests
{
    [TestFixture]
    public class GetUsersTests
    {
        private readonly string _usersUriTest = @"http://api.stackexchange.com/2.2/users";
        private Mock<IFilteringOptionsValidator> _getUsersModelValidator;
        private Mock<IConfigurationHelper> _configurationHelperMock;
        private Mock<IHttpHelper> _httpHelperMock;
        private Mock<IHttpClientHelper> _httpClientHelperMock;
        private Mock<ILogHelper> _logHelperMock;
        private UserService SUT;

        [SetUp]
        public void Setup()
        {
            _getUsersModelValidator = new Mock<IFilteringOptionsValidator>();
            _configurationHelperMock = new Mock<IConfigurationHelper>();
            _httpHelperMock = new Mock<IHttpHelper>();
            _httpClientHelperMock = new Mock<IHttpClientHelper>();
            _logHelperMock = new Mock<ILogHelper>();
        }

        private void InitServiceUnderTest()
        {
            SUT = new UserService(_getUsersModelValidator.Object,
                                  _configurationHelperMock.Object,
                                  _httpHelperMock.Object,
                                  _httpClientHelperMock.Object,
                                  _logHelperMock.Object);
        }

        [Test]
        public void ensure_returns_true_and_users_when_successful()
        {
            // Arrange
            var filteringOptions = new FilteringOptions
            {
                Site = "stackoverflow",
                Order = Order.Desc,
                Sort = Sorting.Reputation,
                PageSize = "20"
            };

            var expectedUri = _usersUriTest;

            var expectedQueryString = "?pagesize=20&sorting=reputation&order=desc&company=stackoverflow";

            var expectedFinalUri = expectedUri + expectedQueryString;

            var expectedGenerationResult = new Result<string>
            {
                Data = expectedQueryString
            };

            var response = new Result<Users>
            {
                Data = new Users { Items = new List<User>() }
            };


            _getUsersModelValidator.Setup(x => x.Validate(filteringOptions)).Returns(new Result());

            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            _httpHelperMock.Setup(x => x.AppendQueryStringParameters(filteringOptions)).Returns(expectedGenerationResult);

            _httpClientHelperMock.Setup(x => x.Get<Users>(expectedFinalUri)).Returns(response);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUsers(filteringOptions);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessful);

            _getUsersModelValidator.Verify(x => x.Validate(filteringOptions), Times.Once);
            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpHelperMock.Verify(x => x.AppendQueryStringParameters(filteringOptions), Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<Users>(expectedFinalUri), Times.Once);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_validation_failed()
        {
            // Arrange
            var filteringOptions = new FilteringOptions
            {
                Site = "stackoverflow",
                Order = Order.Desc,
                Sort = Sorting.Reputation,
                PageSize = "20"
            };

            var expectedErrorMessage = "An error has occured during the validation.";

            var expectedResult = new Result
            {
                Errors = new List<string> {expectedErrorMessage}
            };


            _getUsersModelValidator.Setup(x => x.Validate(filteringOptions)).Returns(expectedResult);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUsers(filteringOptions);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedErrorMessage, result.Errors);
            Assert.IsNull(result.Data);

            _getUsersModelValidator.Verify(x => x.Validate(filteringOptions), Times.Once);
            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Never);
            _httpHelperMock.Verify(x => x.AppendQueryStringParameters(filteringOptions), Times.Never);
            _httpClientHelperMock.Verify(x => x.Get<Users>(It.IsAny<string>()), Times.Never);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_uri_not_found()
        {
            // Arrange
            var filteringOptions = new FilteringOptions
            {
                Site = "stackoverflow",
                Order = Order.Desc,
                Sort = Sorting.Reputation,
                PageSize = "20"
            };

            var expectedUri = "";

            var expectedErrorMessage = "Missing users uri.";


            _getUsersModelValidator.Setup(x => x.Validate(filteringOptions)).Returns(new Result());

            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUsers(filteringOptions);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedErrorMessage, result.Errors);
            Assert.IsNull(result.Data);

            _getUsersModelValidator.Verify(x => x.Validate(filteringOptions), Times.Once);
            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpHelperMock.Verify(x => x.AppendQueryStringParameters(filteringOptions), Times.Never);
            _httpClientHelperMock.Verify(x => x.Get<Users>(It.IsAny<string>()), Times.Never);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_querystring_not_generated()
        {
            // Arrange
            var filteringOptions = new FilteringOptions
            {
                Site = "stackoverflow",
                Order = Order.Desc,
                Sort = Sorting.Reputation,
                PageSize = "20"
            };

            var expectedUri = _usersUriTest;

            var expectedGenerationResult = new Result<string>
            {
                Errors = new List<string> { "An error occured during the queryString generation." }
            };

            _getUsersModelValidator.Setup(x => x.Validate(filteringOptions)).Returns(new Result());

            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            _httpHelperMock.Setup(x => x.AppendQueryStringParameters(filteringOptions)).Returns(expectedGenerationResult);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUsers(filteringOptions);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual(expectedGenerationResult.Errors, result.Errors);
            Assert.IsNull(result.Data);

            _getUsersModelValidator.Verify(x => x.Validate(filteringOptions), Times.Once);
            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpHelperMock.Verify(x => x.AppendQueryStringParameters(filteringOptions), Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<Users>(It.IsAny<string>()), Times.Never);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_request_returns_error()
        {
            // Arrange
            var filteringOptions = new FilteringOptions
            {
                Site = "stackoverflow",
                Order = Order.Desc,
                Sort = Sorting.Reputation,
                PageSize = "20"
            };

            var expectedUri = _usersUriTest;

            var expectedQueryString = "?pagesize=20&sorting=reputation&order=desc&company=stackoverflow";

            var expectedFinalUri = expectedUri + expectedQueryString;

            var expectedGenerationResult = new Result<string>
            {
                Errors = new List<string> { "An error occured during the queryString generation." }
            };

            var expectedError = "An error has occured.";

            var response = new Result<Users>
            {
                Errors = new List<string> { expectedError }
            };


            _getUsersModelValidator.Setup(x => x.Validate(filteringOptions)).Returns(new Result());

            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            _httpHelperMock.Setup(x => x.AppendQueryStringParameters(filteringOptions)).Returns(expectedGenerationResult);

            _httpClientHelperMock.Setup(x => x.Get<Users>(expectedFinalUri)).Returns(response);

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUsers(filteringOptions);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual(expectedGenerationResult.Errors, result.Errors);
            Assert.IsNull(result.Data);

            _getUsersModelValidator.Verify(x => x.Validate(filteringOptions), Times.Once);
            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpHelperMock.Verify(x => x.AppendQueryStringParameters(filteringOptions), Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<Users>(It.IsAny<string>()), Times.Never);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_and_log_when_exception_thrown()
        {
            // Arrange
            var filteringOptions = new FilteringOptions
            {
                Site = "stackoverflow",
                Order = Order.Desc,
                Sort = Sorting.Reputation,
                PageSize = "20"
            };

            var expectedUri = _usersUriTest;

            var expectedQueryString = "?pagesize=20&sorting=reputation&order=desc&company=stackoverflow";

            var expectedGenerationResult = new Result<string>
            {
                Data = expectedQueryString
            };

            var expectedErrorMessage = "An exception has occured.";


            _getUsersModelValidator.Setup(x => x.Validate(filteringOptions)).Returns(new Result());

            _configurationHelperMock.Setup(x => x.UsersRelativeUri).Returns(expectedUri);

            _httpHelperMock.Setup(x => x.AppendQueryStringParameters(filteringOptions)).Returns(expectedGenerationResult);

            _httpClientHelperMock.Setup(x => x.Get<Users>(It.IsAny<string>())).Throws(new Exception(expectedErrorMessage));

            _logHelperMock.Setup(x => x.Exception(It.IsAny<Exception>()));

            InitServiceUnderTest();

            // Act
            var result = SUT.GetUsers(filteringOptions);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedErrorMessage, result.Errors);
            Assert.IsNull(result.Data);

            _getUsersModelValidator.Verify(x => x.Validate(filteringOptions), Times.Once);
            _configurationHelperMock.Verify(x => x.UsersRelativeUri, Times.Once);
            _httpHelperMock.Verify(x => x.AppendQueryStringParameters(filteringOptions), Times.Once);
            _httpClientHelperMock.Verify(x => x.Get<Users>(It.IsAny<string>()), Times.Once);
            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Once); 
        }
    }
}
