﻿using System.Web.Mvc;
using NUnit.Framework;
using WorldRemit.Web.Controllers;

namespace WorldRemit.Tests.Controllers
{
    [TestFixture]
    public class DateControllerTests
    {
        [Test]
        public void Index()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
