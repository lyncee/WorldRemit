﻿using System.Collections.Generic;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using WorldRemit.Services.Interfaces;
using WorldRemit.Services.Models;
using WorldRemit.Shared.Common;
using WorldRemit.Shared.Constants;
using WorldRemit.Web.Controllers.Api;

namespace WorldRemit.Tests.Controllers.Api
{
    [TestFixture]
    public class DateControllerTests
    {
        [Test]
        public void ensure_returns_users_when_retrieving_users_successful()
        {
            // Arrange
            var filteringOptions = new FilteringOptions
            {
                Order = Order.Desc,
                PageSize = "20",
                Site = "stackoverflow",
                Sort = Sorting.Reputation
            };

            var expectedResult = new Result<UsersModel>
            {
                Data = new UsersModel
                {
                    Users = new List<UserModel>
                    {
                        new UserModel(),
                        new UserModel()
                    }
                }
            };

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetUsers(filteringOptions)).Returns(expectedResult);

            var controller = new UserController(userServiceMock.Object);

            // Act
            var result = controller.GetUsers(filteringOptions);
            var contentResult = (OkNegotiatedContentResult<Result<UsersModel>>)result;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            var data = contentResult.Content.Data;
            Assert.IsNotNull(data);
        }

        [Test]
        public void ensure_returns_internal_server_error_when_retrieving_users_failed()
        {
            // Arrange
            var filteringOptions = new FilteringOptions
            {
                Order = Order.Desc,
                PageSize = "20",
                Site = "stackoverflow",
                Sort = Sorting.Reputation
            };

            var expectedErrorMessage = "An error has occured.";

            var expectedResult = new Result<UsersModel>
            {
                Errors = new List<string> { expectedErrorMessage }
            };

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetUsers(filteringOptions)).Returns(expectedResult);

            var controller = new UserController(userServiceMock.Object);

            // Act
            var result = controller.GetUsers(filteringOptions);
            var contentResult = (InternalServerErrorResult)result;

            // Assert
            Assert.IsNotNull(contentResult);
        }

        [Test]
        public void ensure_returns_user_details_when_retrieving_user_details_successful()
        {
            // Arrange
            var userId = "22656";
            var site = "stackoverflow";

            var expectedResult = new Result<UserDetailsModel>
            {
                Data = new UserDetailsModel()
            };

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetUserDetails(userId, site)).Returns(expectedResult);

            var controller = new UserController(userServiceMock.Object);

            // Act
            var result = controller.GetUserDetails(userId, site);
            var contentResult = (OkNegotiatedContentResult<Result<UserDetailsModel>>)result;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            var data = contentResult.Content.Data;
            Assert.IsNotNull(data);
        }

        [Test]
        public void ensure_returns_user_details_when_retrieving_user_details_failed()
        {
            // Arrange
            var userId = "22656";
            var site = "stackoverflow";

            var expectedErrorMessage = "An error has occured.";

            var expectedResult = new Result<UserDetailsModel>
            {
                Errors = new List<string> { expectedErrorMessage }
            };

            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetUserDetails(userId, site)).Returns(expectedResult);

            var controller = new UserController(userServiceMock.Object);

            // Act
            var result = controller.GetUserDetails(userId, site);
            var contentResult = (InternalServerErrorResult)result;

            // Assert
            Assert.IsNotNull(contentResult);
        }
    }
}
