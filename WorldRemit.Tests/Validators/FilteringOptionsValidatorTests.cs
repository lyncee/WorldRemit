﻿using NUnit.Framework;
using WorldRemit.Services.Models;
using WorldRemit.Services.Models.Validators;
using WorldRemit.Shared.Constants;

namespace WorldRemit.Tests.Validators
{
    [TestFixture]
    public class FilteringOptionsValidatorTests
    {
        private FilteringOptionsValidator SUT;

        private void InitServiceUnderTest()
        {
            SUT = new FilteringOptionsValidator();
        }

        [Test]
        public void ensure_returns_true_when_model_valid()
        {
            // Arrange
            var model = new FilteringOptions
            {
                Order = Order.Asc,
                PageSize = "20",
                Site = "stackoverflow",
                Sort = Sorting.Reputation
            };

            InitServiceUnderTest();

            // Act
            var result = SUT.Validate(model);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public void ensure_returns_returns_false_when_order_missing()
        {
            // Arrange
            var model = new FilteringOptions
            {
                Order = "",
                PageSize = "20",
                Site = "stackoverflow",
                Sort = Sorting.Reputation
            };
            var expectedError = "Missing order parameter.";

            InitServiceUnderTest();

            // Act
            var result = SUT.Validate(model);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
        }

        [Test]
        public void ensure_returns_returns_false_when_pageSize_missing()
        {
            // Arrange
            var model = new FilteringOptions
            {
                Order = Order.Asc,
                PageSize = "",
                Site = "stackoverflow",
                Sort = Sorting.Reputation
            };
            var expectedError = "Missing pageSize parameter.";

            InitServiceUnderTest();

            // Act
            var result = SUT.Validate(model);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
        }

        [Test]
        public void ensure_returns_returns_false_when_site_missing()
        {
            // Arrange
            var model = new FilteringOptions
            {
                Order = Order.Asc,
                PageSize = "20",
                Site = "",
                Sort = Sorting.Reputation
            };
            var expectedError = "Missing site parameter.";

            InitServiceUnderTest();

            // Act
            var result = SUT.Validate(model);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
        }

        [Test]
        public void ensure_returns_returns_false_when_sort_missing()
        {
            // Arrange
            var model = new FilteringOptions
            {
                Order = Order.Asc,
                PageSize = "20",
                Site = "stackoverflow",
                Sort = ""
            };
            var expectedError = "Missing sort parameter.";

            InitServiceUnderTest();

            // Act
            var result = SUT.Validate(model);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
        }
    }
}
