﻿using System;
using Moq;
using NUnit.Framework;
using WorldRemit.Services.Models;
using WorldRemit.Shared.Helpers;
using WorldRemit.Shared.Wrappers;

namespace WorldRemit.Tests.Helpers.HttpClientHelperTests
{
    [TestFixture]
    public class GetTests
    {
        private readonly string baseUri = @"baseurl";
        private readonly string endPointUri = @"fakeendpoint";
        private Mock<IConfigurationHelper> _configurationHelperMock;
        private Mock<ILogHelper> _logHelperMock;
        private Mock<IJsonConvertWrapper> _jsonConvertWrapper;
        private Mock<IRestClientWrapper> _restClientWrapper;
        private HttpClientHelper SUT;

        [SetUp]
        public void Setup()
        {
            _configurationHelperMock = new Mock<IConfigurationHelper>();
            _logHelperMock = new Mock<ILogHelper>();
            _jsonConvertWrapper = new Mock<IJsonConvertWrapper>();
            _restClientWrapper = new Mock<IRestClientWrapper>();
        }

        private void InitServiceUnderTest()
        {
            SUT = new HttpClientHelper(_configurationHelperMock.Object, _logHelperMock.Object, _jsonConvertWrapper.Object, _restClientWrapper.Object);
        }

        [Test]
        public void ensure_returns_true_and_object_when_successful()
        {
            // Arrange
            var expectedResponse = "response";
            var expectedObject = new UsersModel();

            _configurationHelperMock.Setup(x => x.ApiRootUri).Returns(baseUri);
            _restClientWrapper.Setup(x => x.Get(baseUri, endPointUri)).Returns(expectedResponse);
            _jsonConvertWrapper.Setup(x => x.Deserialize<UsersModel>(expectedResponse)).Returns(expectedObject);
   
            InitServiceUnderTest();

            // Act
            var result = SUT.Get<UsersModel>(endPointUri);

            // Assert
            Assert.IsTrue(result.IsSuccessful);
            Assert.IsNotNull(result.Data);

            _configurationHelperMock.Verify(x => x.ApiRootUri, Times.Once);
            _restClientWrapper.Verify(x => x.Get(baseUri, endPointUri), Times.Once);
            _jsonConvertWrapper.Verify(x => x.Deserialize<UsersModel>(expectedResponse), Times.Once);
        }

        [Test]
        public void ensure_returns_false_when_exception_thrown()
        {
            // Arrange
            var expectedError = "An error has occured.";

            _configurationHelperMock.Setup(x => x.ApiRootUri).Returns(baseUri);
            _restClientWrapper.Setup(x => x.Get(baseUri, endPointUri)).Throws(new Exception(expectedError));

            InitServiceUnderTest();

            // Act
            var result = SUT.Get<UsersModel>(endPointUri);

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedError, result.Errors);
            Assert.IsNull(result.Data);

            _configurationHelperMock.Verify(x => x.ApiRootUri, Times.Once);
            _restClientWrapper.Verify(x => x.Get(baseUri, endPointUri), Times.Once);
            _jsonConvertWrapper.Verify(x => x.Deserialize<UsersModel>(It.IsAny<string>()), Times.Never);
        }
    }
}
