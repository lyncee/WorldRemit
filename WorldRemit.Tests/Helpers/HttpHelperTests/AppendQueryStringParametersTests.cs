﻿using System;
using Moq;
using NUnit.Framework;
using WorldRemit.Services.Models;
using WorldRemit.Shared.Common;
using WorldRemit.Shared.Constants;
using WorldRemit.Shared.Helpers;

namespace WorldRemit.Tests.Helpers.HttpHelperTests
{
    [TestFixture]
    public class AppendQueryStringParametersTests
    {
        private Mock<ILogHelper> _logHelperMock;
        private HttpHelper SUT;

        [SetUp]
        public void Setup()
        {
            _logHelperMock = new Mock<ILogHelper>();
        }

        private void InitServiceUnderTest()
        {
            SUT = new HttpHelper(_logHelperMock.Object);
        }

        [Test]
        public void ensure_returns_true_and_correct_querystring_when_successful()
        {
            // Arrange
            var model = new FilteringOptions
            {
                Site = "stackoverflow",
                Order = Order.Desc,
                Sort = Sorting.Reputation,
                PageSize = "20"
            };

            var expectedResult = new Result<string>
            {
                Data = $"?order={model.Order}&pagesize={model.PageSize}&site={model.Site}&sort={model.Sort}"
            };

            _logHelperMock.Setup(x => x.Exception(It.IsAny<Exception>()));

            InitServiceUnderTest();

            // Act
            var result = SUT.AppendQueryStringParameters(model);

            // Assert
            Assert.IsTrue(result.IsSuccessful);
            Assert.AreEqual(expectedResult.Message, result.Message);

            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        public void ensure_returns_false_when_object_null()
        {
            // Arrange
            var expectedErrorMessage = "Object is null.";

            InitServiceUnderTest();

            // Act
            var result = SUT.AppendQueryStringParameters(null);

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.Contains(expectedErrorMessage, result.Errors);

            _logHelperMock.Verify(x => x.Exception(It.IsAny<Exception>()), Times.Never);
        }
    }
}
